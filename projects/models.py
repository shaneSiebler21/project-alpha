from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    is_completed = models.BooleanField(default=False)
    image = models.URLField(null=True, blank=True)
    coaching_tips = models.URLField(null=True, blank=True)
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="projects"
    )

    def __str__(self):
        return self.name
