# Generated by Django 4.0.3 on 2022-04-05 23:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0002_remove_task_start_date_task_skill_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='due_date',
        ),
    ]
